<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Company_model extends Model
{
    protected $table = 'hr_companies';
    protected $primaryKey = 'id';
	public $timestamps = false;
//test line

    public function get_companies_list($perpage, $search=array()){
    	$query =  DB::table($this->table)->select($this->table.".*", "region.region_name")->where($this->table.'.deleted',0);
    	if(!empty($search['company_code'])){
    		$query->where('companycode','like', '%'.$search['company_code'].'%');
    	}

		if(!empty($search['company_id'])){
    		$query->where('id',$search['company_id']);
    	}

		if(!empty($search['country_id'])){
    		$query->where('country_id',$search['country_id']);
    	}
		$query->leftJoin('region', 'country_id', '=', 'region_id');
		$query->orderByDesc('id');
    	return $query->paginate($perpage);
    }

	

    
}
