#!/bin/sh
# What's happening here?
#
# 1. We get names and statuses of files that differ in current branch from their state in origin/master.
# These come in form (multiline)
# 2. The output from git diff is filtered by unix grep utility, we only need files with names ending in .php
# 3. One more filter: filter *out* (grep -v) all lines starting with R or D.
# D means "deleted", R means "renamed"
# 4. The filtered status-name list is passed on to awk command, which is instructed to take only the 2nd part
# of every line, thus just the filename

files1=$(git  diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA | grep '\.php$' | grep -v "^[RD]"| awk '{print }')
if [ -z $files1 ] 
then 
  print "No files found"
  print "$?"
else
  print "files were found: $files1" 
fi
# print "Files: $files1"
git  diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA | grep '\.php$' | grep -v "^[RD]"| awk '{print }'

# git diff  ${CI_COMMIT_SHA} master | grep '\.php$' | grep -v "^[RD]" | awk '{ print $2}'
# git diff --name-status origin/master | grep '\.php$' | grep -v "^[RD]" | awk '{ print $2}'
# git diff --name-only HEAD~1 HEAD~2 
