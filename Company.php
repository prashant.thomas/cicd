<?php

namespace App\Hr_Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
	use HasFactory;
	use SoftDeletes;

	protected $softDelete = true;

	protected $table = 'hr_companies';
	 
}
